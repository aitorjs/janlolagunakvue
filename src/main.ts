// meter bootstrap5 https://dev.to/codeply/using-bootstrap-5-with-vue-js-5fnp
// loader https://thewebdev.info/2021/03/28/add-a-loading-spinner-to-a-vue-app-with-the-vue-loading-overlay-component/
// using fetch https://www.techiediaries.com/vue-3-fetch-data-rest-api/ 

import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";

import "./assets/styles.css";

const app = createApp(App);

app.use(router);

app.mount("#app");
